<?php

namespace CompanyBundle\Repository;

use CompanyBundle\Entity\UserCompany;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method UserCompany|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCompany|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCompany[]    findAll()
 * @method UserCompany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCompany::class);
    }
}