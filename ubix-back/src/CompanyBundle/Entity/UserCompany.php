<?php


namespace CompanyBundle\Entity;

use CompanyBundle\Repository\UserCompanyRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;

#[ORM\Entity(repositoryClass: UserCompanyRepository::class)]
#[ORM\Table(name: "user_company")]
class UserCompany extends User
{

    #[ORM\Column]
    private ?array $job = [];

    /**
     * @return array|null
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param array|null $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }
}