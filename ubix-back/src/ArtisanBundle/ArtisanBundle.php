<?php

declare(strict_types=1);

namespace ArtisanBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ArtisanBundle extends Bundle
{
}
