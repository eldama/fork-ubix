<?php


namespace App\Entity;


interface BaseUserInterface
{
    // Types of User
    public const TYPE_ADMIN = 'admin';
    public const TYPE_COMPANY = 'company';
    public const TYPE_CUSTOMER = 'customer';
    public const TYPE_ARTISAN = 'artisan';
    
    public function getEmail(): ?string;
}