<?php

declare(strict_types=1);

namespace CustomerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CustomerBundle extends Bundle
{
}
