<?php

declare(strict_types=1);

namespace CustomerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    #[Route('/')]
    public function indexAction(Request $request): JsonResponse
    {
        return new JsonResponse(['message' => 'hello customer']);
    }
}
