<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    #[Route(path: "/api/auth", name: "api_auth", methods: ["POST"])]
    public function auth()
    {
        dd($this->getUser());
    }
}