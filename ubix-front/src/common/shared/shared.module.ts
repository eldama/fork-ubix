import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'primeng/tooltip';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/translations/', '.json');
}

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        TranslateModule,
        TooltipModule,
    ],
    exports: [
        TranslateModule,
        TooltipModule,
    ],
    providers: [],
})
export class SharedModule {}
