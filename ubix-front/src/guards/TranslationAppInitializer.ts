import { Injector } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LOCATION_INITIALIZED } from '@angular/common';

export function translationInitializerFactory(translate: TranslateService, injector: Injector) {
    return () =>
        new Promise<any>((resolve: any) => {
            const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
            locationInitialized.then(() => {
                const langToSet = 'fr';
                translate.setDefaultLang(langToSet);
                translate.use(langToSet).subscribe(
                    () => {
                        // eslint-disable-next-line no-console
                        console.info(`Successfully initialized '${langToSet}' language.'`);
                    },
                    () => {
                        // eslint-disable-next-line no-console
                        console.error(`Problem with '${langToSet}' language initialization.'`);
                    },
                    () => {
                        resolve(null);
                    },
                );
            });
        });
}
