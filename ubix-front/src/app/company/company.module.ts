import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyComponent } from './company.component';
import { LoginComponent } from './login/login.component';
import { LoggedSpaceModule } from './logged-space/logged-space.module';

@NgModule({
    declarations: [
        CompanyComponent,
        LoginComponent,
    ],
    imports: [
        CommonModule,
        CompanyRoutingModule,
        LoggedSpaceModule,
    ],
})
export class CompanyModule {}
