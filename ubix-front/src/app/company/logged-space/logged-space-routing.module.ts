import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { LoggedSpaceComponent } from './logged-space.component';

const routes: Routes = [
    {
        path: '',
        component: LoggedSpaceComponent,
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full',
            },
            {
                path: 'list',
                component: ProjectListComponent,
                pathMatch: 'full',
            },
            {
                path: 'settings',
                loadChildren: () => import('./setting/setting.module').then((m) => m.SettingModule),
            },
            {
                path: 'project/:id',
                loadChildren: () =>
                    import('./project-details/project-details.module').then((m) => m.ProjectDetailsModule),
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LoggedSpaceRoutingModule {}
