import { Component } from '@angular/core';
import { ProjectService } from '../../../../service/project.service';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.scss'],
})
export class ProjectListComponent {
    checked = false;

    constructor(private projectService: ProjectService) {}
}
