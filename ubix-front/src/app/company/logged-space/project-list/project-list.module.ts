import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectListComponent } from './project-list.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../common/shared/shared.module';

@NgModule({
    declarations: [
        ProjectListComponent,
    ],
    imports: [
        CommonModule,
        InputSwitchModule,
        FormsModule,
        SharedModule,
    ],
    exports: [
        ProjectListComponent,
    ],
})
export class ProjectListModule {}
