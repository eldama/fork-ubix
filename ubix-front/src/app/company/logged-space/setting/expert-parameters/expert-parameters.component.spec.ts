import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertParametersComponent } from './expert-parameters.component';

describe('ExpertParametersComponent', () => {
    let component: ExpertParametersComponent;
    let fixture: ComponentFixture<ExpertParametersComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ExpertParametersComponent],
        });
        fixture = TestBed.createComponent(ExpertParametersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
