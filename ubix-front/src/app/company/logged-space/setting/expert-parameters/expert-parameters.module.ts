import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpertParametersComponent } from './expert-parameters.component';
import { SharedModule } from '../../../../../common/shared/shared.module';

@NgModule({
    declarations: [
        ExpertParametersComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        ExpertParametersComponent,
    ],
})
export class ExpertParametersModule {}
