import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedParametersComponent } from './advanced-parameters.component';

describe('AdvancedParametersComponent', () => {
    let component: AdvancedParametersComponent;
    let fixture: ComponentFixture<AdvancedParametersComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AdvancedParametersComponent],
        });
        fixture = TestBed.createComponent(AdvancedParametersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
