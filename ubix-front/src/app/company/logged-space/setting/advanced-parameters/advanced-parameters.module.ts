import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvancedParametersComponent } from './advanced-parameters.component';
import { SharedModule } from '../../../../../common/shared/shared.module';
import { CrmComponent } from './crm/crm.component';
import { InputSwitchModule } from 'primeng/inputswitch';

@NgModule({
    declarations: [
        AdvancedParametersComponent,
        CrmComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        InputSwitchModule,
    ],
    exports: [
        AdvancedParametersComponent,
    ],
})
export class AdvancedParametersModule {}
