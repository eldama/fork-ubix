import { Component } from '@angular/core';

@Component({
    selector: 'app-advanced-parameters',
    templateUrl: './advanced-parameters.component.html',
    styleUrls: ['./advanced-parameters.component.scss'],
})
export class AdvancedParametersComponent {}
