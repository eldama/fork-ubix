import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { EventType, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SettingComponent implements OnInit, OnDestroy {
    tabs: MenuItem[] = [];
    activeItem: MenuItem;
    routerSubscription: Subscription;
    settingPath = '/settings/';

    constructor(
        private router: Router,
        private translateService: TranslateService,
    ) {}

    ngOnDestroy(): void {
        this.routerSubscription.unsubscribe();
    }
    ngOnInit(): void {
        this.tabs = [
            {
                id: 'profile',
                label: this.translateService.instant('company.settings.menu.profile'),
                command: () => {
                    this.router.navigate(['/settings/profile']);
                },
            },
            {
                id: 'society',
                label: this.translateService.instant('company.settings.menu.society'),
                command: () => {
                    this.router.navigate(['/settings/society']);
                },
            },
            {
                id: 'steps',
                label: this.translateService.instant('company.settings.menu.clientStep'),
                command: () => {
                    this.router.navigate(['/settings/steps']);
                },
            },
            // {
            //     label: this.translateService.instant('company.settings.menu.customerAdvice'),
            //     command: () => {
            //         this.router.navigate(['/settings/customer-advice']);
            //     },
            // },
            {
                id: 'parameters',
                label: this.translateService.instant('company.settings.menu.parameters'),
                command: () => {
                    this.router.navigate(['/settings/parameters']);
                },
            },
            {
                id: 'advanced-parameters',
                label: this.translateService.instant('company.settings.menu.advancedParameters'),
                command: () => {
                    this.router.navigate(['/settings/advanced-parameters']);
                },
            },
            {
                id: 'expert-parameters',
                label: this.translateService.instant('company.settings.menu.expertParameters'),
                command: () => {
                    this.router.navigate(['/settings/expert-parameters']);
                },
            },
        ];
        this.routerSubscription = this.router.events.subscribe((value) => {
            if (value.type == EventType.Scroll) {
                if (value.routerEvent instanceof NavigationEnd) {
                    this.onRouteChange(value.routerEvent.urlAfterRedirects);
                } else {
                    this.onRouteChange(value.routerEvent.url);
                }
            } else if (value.type == EventType.NavigationEnd) {
                this.onRouteChange(value.urlAfterRedirects ?? value.url);
            }
        });
    }

    private onRouteChange(url: string) {
        this.tabs.forEach((menuItem) => {
            if (url.startsWith(this.settingPath + menuItem.id)) {
                this.activeItem = menuItem;
            }
        });
    }
}
