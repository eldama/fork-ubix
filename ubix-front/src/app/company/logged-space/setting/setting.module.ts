import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingRoutingModule } from './setting-routing.module';
import { SettingComponent } from './setting.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { AdvancedParametersModule } from './advanced-parameters/advanced-parameters.module';
import { ExpertParametersModule } from './expert-parameters/expert-parameters.module';
import { ProfileModule } from './profile/profile.module';
import { ParametersModule } from './parameters/parameters.module';
import { SettingStepModule } from './setting-step/setting-step.module';
import { LostProjectModule } from './parameters/lost-project/lost-project.module';

@NgModule({
    declarations: [
        SettingComponent,
    ],
    imports: [
        CommonModule,
        TabMenuModule,
        SettingRoutingModule,
        AdvancedParametersModule,
        ExpertParametersModule,
        ProfileModule,
        ParametersModule,
        SettingStepModule,
        LostProjectModule,
    ],
})
export class SettingModule {}
