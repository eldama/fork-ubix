import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingComponent } from './setting.component';
import { AdvancedParametersComponent } from './advanced-parameters/advanced-parameters.component';
import { ExpertParametersComponent } from './expert-parameters/expert-parameters.component';
import { ProfileComponent } from './profile/profile.component';
import { SocietyComponent } from './society/society.component';
import { ParametersComponent } from './parameters/parameters.component';
import { SettingStepComponent } from './setting-step/setting-step.component';

const routes: Routes = [
    {
        path: '',
        component: SettingComponent,
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full',
            },
            {
                path: 'profile',
                component: ProfileComponent,
            },
            {
                path: 'society',
                component: SocietyComponent,
            },
            {
                path: 'steps',
                component: SettingStepComponent,
            },
            {
                path: 'parameters',
                component: ParametersComponent,
            },
            {
                path: 'advanced-parameters',
                component: AdvancedParametersComponent,
            },
            {
                path: 'expert-parameters',
                component: ExpertParametersComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SettingRoutingModule {}
