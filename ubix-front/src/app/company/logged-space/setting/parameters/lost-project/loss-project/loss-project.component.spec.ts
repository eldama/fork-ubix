import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LossProjectComponent } from './loss-project.component';

describe('LossProjectComponent', () => {
    let component: LossProjectComponent;
    let fixture: ComponentFixture<LossProjectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LossProjectComponent],
        });
        fixture = TestBed.createComponent(LossProjectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
