import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LossChildProjectComponent } from './loss-child-project.component';

describe('LossChildProjectComponent', () => {
    let component: LossChildProjectComponent;
    let fixture: ComponentFixture<LossChildProjectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LossChildProjectComponent],
        });
        fixture = TestBed.createComponent(LossChildProjectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
