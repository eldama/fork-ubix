import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-loss-child-project',
    templateUrl: './loss-child-project.component.html',
    styleUrls: ['./loss-child-project.component.scss'],
})
export class LossChildProjectComponent {
    @Input() itemValue = '';
    @Input() itemId: number;
    isEllipsisVisible = false;
    isCheckisVisible = false;
    isMenuVisible = true;
    isEditing = false;

    onEllipsisClick() {
        this.isEllipsisVisible = !this.isEllipsisVisible;
    }

    toggleEdit() {
        this.isEditing = !this.isEditing;
        if (this.isEditing) {
            this.isEllipsisVisible = false;
            this.isCheckisVisible = true;
            this.isMenuVisible = false;
        }
    }

    validateInput() {
        this.isMenuVisible = true;
        this.isCheckisVisible = false;
        this.isEllipsisVisible = false;
        this.isEditing = false;
        //get value in input by console.log(this.itemValue);
        //get value in input by console.log(this.itemId);
    }
}
