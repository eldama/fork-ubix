import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LossProjectComponent } from './loss-project/loss-project.component';
import { LossChildProjectComponent } from './loss-child-project/loss-child-project.component';
import { SharedModule } from 'src/common/shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        LossProjectComponent,
        LossChildProjectComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
    ],
    exports: [
        LossProjectComponent,
    ],
})
export class LostProjectModule {}
