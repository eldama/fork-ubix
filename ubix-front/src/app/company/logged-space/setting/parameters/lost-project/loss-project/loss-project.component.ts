import { Component } from '@angular/core';

@Component({
    selector: 'app-loss-project',
    templateUrl: './loss-project.component.html',
    styleUrls: ['./loss-project.component.scss'],
})
export class LossProjectComponent {
    items: { id: number; value: string }[] = [
        { id: 1, value: 'Problèmes de gestion des coûts' },
        { id: 2, value: 'Autre problème' },
        { id: 3, value: 'Encore un problème' },
        { id: 4, value: 'Et encore un problème' },
        { id: 5, value: 'Des problèmes' },
        { id: 6, value: 'Beaucoup de problèmes...' },
    ];
    newItemValue = '';

    addItem(value: string) {
        if (value.trim() !== '') {
            const newItem = {
                id: this.items.length + 1,
                value: value.trim(),
            };
            this.items.push(newItem);
            this.newItemValue = '';
        }
    }
}
