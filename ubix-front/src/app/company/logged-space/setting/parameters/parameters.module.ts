import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParametersComponent } from '../parameters/parameters.component';
import { SharedModule } from '../../../../../common/shared/shared.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { LostProjectModule } from './lost-project/lost-project.module';
@NgModule({
    declarations: [
        ParametersComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        InputSwitchModule,
        FormsModule,
        LostProjectModule,
    ],
    exports: [
        ParametersComponent,
    ],
})
export class ParametersModule {}
