import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocietyComponent } from './society.component';
import { SharedModule } from '../../../../../common/shared/shared.module';

@NgModule({
    declarations: [
        SocietyComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        SocietyComponent,
    ],
})
export class SocietyModule {}
