import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '../../../../../common/shared/shared.module';

@NgModule({
    declarations: [
        ProfileComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        ProfileComponent,
    ],
})
export class ProfileModule {}
