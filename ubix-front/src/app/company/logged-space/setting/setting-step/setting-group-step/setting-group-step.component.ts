import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-setting-group-step',
    templateUrl: './setting-group-step.component.html',
    styleUrls: ['./setting-group-step.component.scss'],
})
export class SettingGroupStepComponent {
    @ViewChild('editStepNameField') inputField: ElementRef;
    collapsed = true;
    editMainStepName = false;
    mainStepName = 'Bienvenue';
    temps = [
        "Conseils pour la recherche d'un terrain",
        'RDV découverte',
        'Avant projet',
    ];

    toggleCollapse() {
        this.collapsed = !this.collapsed;
    }

    onEditMainStepNameClick(event: MouseEvent) {
        event.stopPropagation();
        this.editMainStepName = !this.editMainStepName;
        this.inputField.nativeElement.focus();
    }

    validEditName() {
        this.editMainStepName = false;
    }

    keyUp($event: KeyboardEvent) {
        if ($event.code == 'Enter') {
            this.validEditName();
        }
    }
}
