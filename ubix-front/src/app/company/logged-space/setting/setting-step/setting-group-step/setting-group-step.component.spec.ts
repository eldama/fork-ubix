import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingGroupStepComponent } from './setting-group-step.component';

describe('SettingGroupStepComponent', () => {
    let component: SettingGroupStepComponent;
    let fixture: ComponentFixture<SettingGroupStepComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SettingGroupStepComponent],
        });
        fixture = TestBed.createComponent(SettingGroupStepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
