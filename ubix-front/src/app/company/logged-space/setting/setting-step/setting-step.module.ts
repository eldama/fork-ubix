import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingStepComponent } from './setting-step.component';
import { SharedModule } from '../../../../../common/shared/shared.module';
import { SettingGroupStepComponent } from './setting-group-step/setting-group-step.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { SettingChildStepComponent } from './setting-child-step/setting-child-step.component';
import { OrderListModule } from 'primeng/orderlist';

@NgModule({
    declarations: [
        SettingStepComponent,
        SettingGroupStepComponent,
        SettingChildStepComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        NgbCollapseModule,
        InputSwitchModule,
        FormsModule,
        OrderListModule,
    ],
    exports: [
        SettingStepComponent,
    ],
})
export class SettingStepModule {}
