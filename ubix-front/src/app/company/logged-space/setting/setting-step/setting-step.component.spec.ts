import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingStepComponent } from './setting-step.component';

describe('SettingStepComponent', () => {
    let component: SettingStepComponent;
    let fixture: ComponentFixture<SettingStepComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SettingStepComponent],
        });
        fixture = TestBed.createComponent(SettingStepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
