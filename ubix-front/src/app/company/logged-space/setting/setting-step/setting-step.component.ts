import { Component, ViewEncapsulation } from '@angular/core';
import { Environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-setting-step',
    templateUrl: './setting-step.component.html',
    styleUrls: ['./setting-step.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SettingStepComponent {
    protected readonly Environment = Environment;
}
