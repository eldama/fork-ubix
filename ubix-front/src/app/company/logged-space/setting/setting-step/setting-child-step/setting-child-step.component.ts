import { Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
    selector: 'app-setting-child-step',
    templateUrl: './setting-child-step.component.html',
    styleUrls: ['./setting-child-step.component.scss'],
})
export class SettingChildStepComponent {
    @ViewChild('editStepNameField') inputField: ElementRef;
    @Input() name: string;
    editStepName = false;
    onEditMainStepNameClick(event: MouseEvent) {
        event.stopPropagation();
        this.editStepName = !this.editStepName;
        this.inputField.nativeElement.focus();
    }

    validEditName() {
        this.editStepName = false;
    }

    keyUp($event: KeyboardEvent) {
        if ($event.code == 'Enter') {
            this.validEditName();
        }
    }
}
