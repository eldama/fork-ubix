import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingChildStepComponent } from './setting-child-step.component';

describe('SettingChildStepComponent', () => {
    let component: SettingChildStepComponent;
    let fixture: ComponentFixture<SettingChildStepComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SettingChildStepComponent],
        });
        fixture = TestBed.createComponent(SettingChildStepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
