import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EventType, NavigationEnd, Router } from '@angular/router';

export enum LeftMenu {
    projects,
    settings,
}

@Component({
    selector: 'app-logged-space',
    templateUrl: './logged-space.component.html',
    styleUrls: ['./logged-space.component.scss'],
})
export class LoggedSpaceComponent implements OnInit, OnDestroy {
    routerSubscription: Subscription;
    activeMenu: number;
    toggled = false;

    constructor(private router: Router) {}
    ngOnDestroy(): void {
        this.routerSubscription.unsubscribe();
    }
    ngOnInit(): void {
        this.routerSubscription = this.router.events.subscribe((value) => {
            if (value.type == EventType.Scroll) {
                if (value.routerEvent instanceof NavigationEnd) {
                    this.onRouteChange(value.routerEvent.urlAfterRedirects);
                } else {
                    this.onRouteChange(value.routerEvent.url);
                }
            } else if (value.type == EventType.NavigationEnd) {
                this.onRouteChange(value.urlAfterRedirects ?? value.url);
            }
        });
    }
    toggledMenu(e: Event) {
        e.preventDefault();
        this.toggled = !this.toggled;
    }

    private onRouteChange(url: string) {
        if (url.startsWith('/list')) {
            this.activeMenu = LeftMenu.projects;
        } else if (url.startsWith('/settings')) {
            this.activeMenu = LeftMenu.settings;
        }
    }

    protected readonly LeftMenu = LeftMenu;
}
