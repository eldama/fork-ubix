import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-logged-top-bar',
    templateUrl: './logged-top-bar.component.html',
    styleUrls: ['./logged-top-bar.component.scss'],
})
export class LoggedTopBarComponent {
    @Output() toggledMenu = new EventEmitter<Event>();
    onToggledMenu($event: MouseEvent) {
        this.toggledMenu.emit($event);
    }
}
