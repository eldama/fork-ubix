import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedTopBarComponent } from './logged-top-bar.component';

describe('LoggedTopBarComponent', () => {
    let component: LoggedTopBarComponent;
    let fixture: ComponentFixture<LoggedTopBarComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LoggedTopBarComponent],
        });
        fixture = TestBed.createComponent(LoggedTopBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
