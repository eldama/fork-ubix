import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedSpaceComponent } from './logged-space.component';

describe('LoggedSpaceComponent', () => {
    let component: LoggedSpaceComponent;
    let fixture: ComponentFixture<LoggedSpaceComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LoggedSpaceComponent],
        });
        fixture = TestBed.createComponent(LoggedSpaceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
