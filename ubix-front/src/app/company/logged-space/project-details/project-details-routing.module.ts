import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectDetailsComponent } from './project-details.component';
import { FilProjectComponent } from './fil-project/fil-project.component';

const routes: Routes = [
    {
        path: '',
        component: ProjectDetailsComponent,
        children: [
            {
                path: '',
                component: FilProjectComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProjectDetailsRoutingModule {}
