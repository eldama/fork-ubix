import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../../common/shared/shared.module';
import { FilProjectComponent } from './fil-project.component';

@NgModule({
    declarations: [
        FilProjectComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        FilProjectComponent,
    ],
})
export class FilProjectModule {}
