import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilProjectComponent } from './fil-project.component';

describe('FilProjectComponent', () => {
    let component: FilProjectComponent;
    let fixture: ComponentFixture<FilProjectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FilProjectComponent],
        });
        fixture = TestBed.createComponent(FilProjectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
