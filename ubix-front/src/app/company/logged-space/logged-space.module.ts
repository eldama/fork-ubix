import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoggedSpaceRoutingModule } from './logged-space-routing.module';
import { LoggedSpaceComponent } from './logged-space.component';
import { SharedModule } from '../../../common/shared/shared.module';
import { LoggedTopBarComponent } from './components/logged-top-bar/logged-top-bar.component';
import { ProjectListModule } from './project-list/project-list.module';

@NgModule({
    imports: [
        CommonModule,
        LoggedSpaceRoutingModule,
        SharedModule,
        ProjectListModule,
    ],
    declarations: [
        LoggedSpaceComponent,
        LoggedTopBarComponent,
    ],
})
export class LoggedSpaceModule {}
