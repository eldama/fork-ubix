import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CompanyComponent } from './company.component';

const routes: Routes = [
    {
        path: '',
        component: CompanyComponent,
        children: [
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: '',
                loadChildren: () => import('./logged-space/logged-space.module').then((m) => m.LoggedSpaceModule),
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CompanyRoutingModule {}
