import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Environment } from '../environments/environment';

const companyRoutes: Routes = [
    {
        path: '',
        loadChildren: () => import('./company/company.module').then((m) => m.CompanyModule),
    },
];
const customerRoutes: Routes = [
    {
        path: '',
        loadChildren: () => import('./customer/customer.module').then((m) => m.CustomerModule),
    },
];
let routes: Routes = [];
if (window.location.host.includes(Environment.companyDomain)) {
    routes = companyRoutes;
} else if (window.location.host.includes(Environment.customerDomain)) {
    routes = customerRoutes;
}
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
