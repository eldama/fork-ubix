import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerSpaceRoutingModule } from './customer-space-routing.module';
import { HomeComponent } from './home/home.component';
import { CustomerSpaceComponent } from './customer-space.component';

@NgModule({
    declarations: [
        HomeComponent,
        CustomerSpaceComponent,
    ],
    imports: [
        CommonModule,
        CustomerSpaceRoutingModule,
    ],
})
export class CustomerSpaceModule {}
