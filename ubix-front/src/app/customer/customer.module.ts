import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { LoginComponent } from './login/login.component';

@NgModule({
    declarations: [
        CustomerComponent,
        LoginComponent,
    ],
    imports: [
        CommonModule,
        CustomerRoutingModule,
    ],
})
export class CustomerModule {}
