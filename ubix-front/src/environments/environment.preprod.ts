export const Environment = {
    customerDomain: 'preprod.portal.ubix-solar.com',
    companyDomain: 'preprod.ubix-solar.com',
    contactMail: 'contact@ubix-solar.com',
    supportMail: 'support@ubix-solar.com',
    supportPhone: '09 72 36 48 00',
};
