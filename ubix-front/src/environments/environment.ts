export const Environment = {
    customerDomain: 'portal.ubix-solar.mg',
    companyDomain: 'www.ubix-solar.mg',
    contactMail: 'contact@ubix-solar.com',
    supportMail: 'support@ubix-solar.com',
    supportPhone: '09 72 36 48 00',
};
