export const Environment = {
    customerDomain: 'staging.portal.ubix-solar.com',
    companyDomain: 'staging.ubix-solar.com',
    contactMail: 'contact@ubix-solar.com',
    supportMail: 'support@ubix-solar.com',
    supportPhone: '09 72 36 48 00',
};
