module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
    },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:prettier/recommended',
        'plugin:@angular-eslint/recommended',
        'plugin:@angular-eslint/template/process-inline-templates',
    ],
    rules: {
        '@angular-eslint/directive-selector': [
            'error',
            {
                type: 'attribute',
                prefix: 'app',
                style: 'camelCase',
            },
        ],
        '@angular-eslint/component-selector': [
            'error',
            {
                type: 'element',
                prefix: 'app',
                style: 'kebab-case',
            },
        ],
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-unused-vars': 2,
        'no-console': 2,
    },
    overrides: [
        {
            files: ['*.ts'],
            rules: {
                'prettier/prettier': [
                    'error',
                    {
                        configFile: './.prettierrc.js',
                    },
                ],
            },
        },
        {
            files: ['*.html'],
            parser: '@angular-eslint/template-parser',
            parserOptions: {
                project: './tsconfig.app.json',
                ecmaVersion: 2020,
                sourceType: 'module',
            },
            extends: [
                'plugin:@angular-eslint/template/recommended',
                'plugin:@angular-eslint/template/accessibility',
            ],
            rules: {
                'prettier/prettier': [
                    'error',
                    {
                        configFile: './.prettierrc.js',
                    },
                ],
                '@angular-eslint/template/click-events-have-key-events': 'off',
                '@angular-eslint/template/interactive-supports-focus': 'off',
            },
        },
    ],
};
