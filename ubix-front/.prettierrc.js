module.exports = {
    trailingComma: 'all',
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    multilineArraysWrapThreshold: 2,
    plugins: [
        // relative paths are usually required, in my experience, so Prettier can find the plugin
        './node_modules/prettier-plugin-multiline-arrays/dist/index.js',
    ],
    printWidth: 120,
}
