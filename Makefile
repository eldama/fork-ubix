DOCKER_COMPOSE_FILE = docker-compose.yml
COMPOSE_PROJECT_NAME = ubix
DOCKER_COMPOSE = docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(DOCKER_COMPOSE_FILE)

DOCKER_COMPOSE_EXEC = $(DOCKER_COMPOSE) exec -T
DOCKER_COMPOSE_UP_OPTIONS = -d --remove-orphans
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up $(DOCKER_COMPOSE_UP_OPTIONS)

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

start:
	$(DOCKER_COMPOSE) stop
	$(DOCKER_COMPOSE_UP)

install:
	$(DOCKER_COMPOSE) build
	cd ubix-back && make install
	cd ubix-front && make npm-install
	$(DOCKER_COMPOSE_UP)